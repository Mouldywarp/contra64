﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerMovement : MonoBehaviour
{
    [SerializeField]
    private PlayerPhysics m_Physics;

    [SerializeField]
    private Transform m_Root;

    [SerializeField]
    private float m_Speed = 10;

    [SerializeField]
    private float m_TurnLerp = 10;

    /// <summary>
    /// TODO To Component
    /// </summary>
    public void DoMovement(Vector3 moveDirection)
    {
        // Movement
        Vector3 velocity = m_Physics.Velocity;
        velocity.x = moveDirection.x * m_Speed;
        velocity.z = moveDirection.z * m_Speed;
        m_Physics.Velocity = velocity;

        // Rotation
        if (moveDirection.sqrMagnitude > 0.0001f)
        {
            Quaternion desired = Quaternion.LookRotation(moveDirection);
            m_Root.rotation = Quaternion.Lerp(m_Root.rotation, desired, m_TurnLerp * Time.deltaTime);
        }

    }
}
