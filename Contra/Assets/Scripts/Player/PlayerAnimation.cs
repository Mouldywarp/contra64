﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private PlayerPhysics m_PlayerPhysics;

    [SerializeField]
    private Weapon m_PlayerWeapon;

    [SerializeField]
    private Transform m_AimTransform;




    /// <summary>
    /// Update
    /// </summary>
    void Update()
    {
        m_Animator.SetBool("Running", m_PlayerPhysics.Velocity.sqrMagnitude > 0.01f);
    }

    private void LateUpdate()
    {
        // This is dodgy formular makes the gun point in vaguley the right direction
        // whilst aiming (without an animator to help me do it properly!)
        Vector3 euler = m_AimTransform.localRotation.eulerAngles;
        euler.z = -m_PlayerWeapon.VerticalAimAngle;
        m_AimTransform.localRotation = Quaternion.Euler(euler);
    }
}
