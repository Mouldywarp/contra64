﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : MonoBehaviour
{
    [SerializeField]
    private Transform m_GunTransform;

    [SerializeField]
    private ParticleSystem m_ParticleSystem;

    [SerializeField]
    Vector3 m_Offset;

    [SerializeField]
    private float m_MaxDistance = 1000;

    [SerializeField]
    private int m_Damage = 1;

    [SerializeField]
    private float m_CoolDown = 0.2f;

    private IEnumerator m_ShootingRoutine;
    Transform m_Transform;
    private int m_Mask;

    /// <summary>
    /// Get the point the bullets shoot from, in world space
    /// </summary>
    public Vector3 Origin
    {
        get { return m_Transform.TransformPoint(m_Offset); }
    }

    private void Awake()
    {
        m_Transform = transform;
        m_Mask = 1 << LayerMask.NameToLayer("Shootable");
    }

    private void OnDisable()
    {
        m_ParticleSystem.Stop();
        m_ShootingRoutine = null;
    }

    public void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(Origin, m_Transform.forward, out hit, m_MaxDistance, m_Mask))
        {
            Shootable shootable = hit.collider.GetComponent<Shootable>();
            shootable.Shoot(m_Damage);
        }
        m_ParticleSystem.transform.position = m_GunTransform.position;

        if (m_ShootingRoutine == null)
        {
            m_ParticleSystem.Play();            
        }
        else
        {
            StopCoroutine(m_ShootingRoutine);
        }
        m_ShootingRoutine = ShootingRoutine();
        StartCoroutine(m_ShootingRoutine);
    }

    private IEnumerator ShootingRoutine()
    {
        yield return new WaitForSeconds(m_CoolDown);
        m_ParticleSystem.Stop();
        m_ShootingRoutine = null;
    }



    //private void OnDrawGizmos()
    //{
    //    if (Application.isPlaying)
    //    {
    //        Vector3 lOrigin = Origin;


    //        Gizmos.DrawLine(lOrigin, lOrigin + m_Transform.forward * 100);
    //    }
    //}
}
