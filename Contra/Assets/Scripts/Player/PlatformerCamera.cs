﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerCamera : MonoBehaviour
{
    [SerializeField]
    private Transform m_CameraTransform;

    [SerializeField]
    private Transform m_Target;

    [SerializeField]
    private float m_Distance;

    [SerializeField]
    private float m_Height;

    [SerializeField]
    private float m_Sensitivity = 180;

    [SerializeField]
    private float m_TransitionLerpFactor = 20;

    private bool m_InTransition;

    private void OnEnable()
    {
        m_InTransition = true;
    }

    void LateUpdate()
    {
        Vector3 target = m_Target.position;
        target.y += m_Height;

        if ( m_InTransition )
        {
            target -= m_CameraTransform.forward * m_Distance;
            m_CameraTransform.position = Vector3.Lerp( m_CameraTransform.position, target, m_TransitionLerpFactor * Time.deltaTime );
            m_InTransition = ( target - m_CameraTransform.position ).sqrMagnitude > 0.3f;
            return;
        }

        m_CameraTransform.position = target;

        m_CameraTransform.Rotate(Vector3.up, Input.GetAxis("CameraX") * m_Sensitivity * Time.deltaTime, Space.World);
        m_CameraTransform.Rotate(Vector3.right, Input.GetAxis("CameraY") * m_Sensitivity * Time.deltaTime, Space.Self);

        m_CameraTransform.position -= m_CameraTransform.forward * m_Distance;
    }
}
