﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplatoonCamera : MonoBehaviour
{
    [SerializeField]
    private Transform m_CameraTransform;

    [SerializeField]
    private Transform m_Target;

    [SerializeField]
    private Transform m_AimTransform;

    [SerializeField]
    private float m_Distance;

    [SerializeField]
    private float m_Height;

    [SerializeField]
    private float m_Sensitivity = 180;

    [SerializeField]
    private float m_TransitionLerpFactor = 20;

    private bool m_InTransition;

    private void OnEnable()
    {
        m_InTransition = true;
    }


    void LateUpdate()
    {
        Vector3 target = m_Target.position;
        target.y += m_Height;

        if(m_InTransition)
        {
            float currentDistance = (m_CameraTransform.position - target).magnitude;
            float distance = Mathf.Lerp( currentDistance, m_Distance, m_TransitionLerpFactor * Time.deltaTime );

            m_CameraTransform.position = target;

            m_CameraTransform.rotation = Quaternion.Slerp( 
                m_CameraTransform.rotation, m_AimTransform.rotation, m_TransitionLerpFactor * Time.deltaTime );

            m_CameraTransform.position -= m_CameraTransform.forward * distance;

            m_InTransition = Mathf.Abs( distance - m_Distance ) > 0.1f;
            return;
        }

        m_CameraTransform.position = target;

        m_CameraTransform.rotation = m_AimTransform.rotation;

        m_CameraTransform.position -= m_CameraTransform.forward * m_Distance;
    }
}
