﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private float m_AimSensitivity = 180;

    [SerializeField]
    private float m_MaxAngle = 45;

    [SerializeField]
    private MachineGun m_CurrentWeapon; // todo make some system to swap weapons

    Transform m_Transform;

    public float VerticalAimAngle
    {
        set
        {
            m_Transform.localRotation = Quaternion.Euler(value, 0, 0);
        }
        get
        {
            return m_Transform.localRotation.eulerAngles.x < 180 ?
                m_Transform.localRotation.eulerAngles.x :
                m_Transform.localRotation.eulerAngles.x - 360;
        }
    }

    private void Awake()
    {
        m_Transform = transform;
    }

    public void Shoot()
    {
        m_CurrentWeapon.Shoot();
    }

    /// <summary>
    /// Aim up or down based on input
    /// </summary>
    /// <param name="input">Value from -1 to 1 indicating if aiming up or down</param>
    public void Aim(float input)
    {
        VerticalAimAngle = Mathf.Clamp(
            VerticalAimAngle + input * m_AimSensitivity * Time.deltaTime, 
            -m_MaxAngle,
            m_MaxAngle);
    }
}
