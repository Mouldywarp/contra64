﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_CameraBehaviours;


    public void SetType(int idx)
    {
        for(int i = 0; i < m_CameraBehaviours.Length; i++)
        {
            m_CameraBehaviours[i].SetActive(i == idx);
        }
    }
}
