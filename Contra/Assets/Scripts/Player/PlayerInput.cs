﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    private PlatformerMovement m_PlatformMovement;

    [SerializeField]
    private SplatoonMovement m_SplatoonMovement;

    [SerializeField]
    private PlayerPhysics m_Physics;

    [SerializeField]
    private Weapon m_Weapon;

    [SerializeField]
    private CameraController m_Camera;

    const int CAM_PLATFORM = 0;
    const int CAM_SPLATOON = 1;
    private int m_CameraType = CAM_PLATFORM;

    /// <summary>
    /// Do the update dance
    /// </summary>
    void Update()
    {
        // Aim button
        int type = Mathf.Max(0, Mathf.CeilToInt(Input.GetAxis("Aim")));
        if(type != m_CameraType )
        {
            m_CameraType = type;
            m_Camera.SetType(m_CameraType);
        }        

        // Movement Input
        Vector3 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 forward = Vector3.Cross(m_Camera.transform.right, Vector3.up);
        Vector3 moveDirection = ((forward * input.y) + (m_Camera.transform.right * input.x)).normalized;
        if (m_CameraType == CAM_PLATFORM)
        {
            m_PlatformMovement.DoMovement(moveDirection);
            m_Weapon.VerticalAimAngle = 0;
        }
        else
        {
            m_SplatoonMovement.DoMovement(moveDirection);
            m_Weapon.Aim(Input.GetAxis("CameraY"));
        }

        // Jump
        if(Input.GetButtonDown("Jump"))
        {
            m_Physics.Jump();
        }
        
        // Shoot
        if(Input.GetButton("Shoot") || m_CameraType == CAM_SPLATOON)
        {
            m_Weapon.Shoot();
        }
    }
}
