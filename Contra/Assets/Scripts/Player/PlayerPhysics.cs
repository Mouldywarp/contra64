﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysics : MonoBehaviour
{
    const int MAX_COLLISIONS = 10;

    [SerializeField]
    private float m_TerminalVelocity;

    [SerializeField]
    private float m_Gravity = -9;

    [SerializeField]
    private float m_GroundSnapDistance = 0.25f;

    [SerializeField]
    private float m_JumpForce = 10;

    [SerializeField]
    private float m_JumpUnstickTime = 0.1f;

    private Transform m_Transform;
    private Rigidbody m_Rigidbody;
    private CapsuleCollider m_Collider;

    private int m_CollisionMask;
    private Collider[] m_Collisions = new Collider[MAX_COLLISIONS];
    private const float m_PhysicsFrameRate = 1.0f / 30.0f;

    private Vector3 m_Velocity;
    private bool m_OnGround;
    private float m_JumpingTime;

    public Vector3 Velocity
    {
        get { return m_Velocity; }
        set { m_Velocity = value; }
    }

    void Awake()
    {
        m_Transform = transform;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Collider = GetComponentInChildren<CapsuleCollider>();
    }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        m_CollisionMask = 1 << LayerMask.NameToLayer("Player");
        m_CollisionMask = ~m_CollisionMask;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        // If there's a frame drop we need to ensure the physics catches up
        int numberOfIterations = Mathf.CeilToInt(Time.deltaTime / m_PhysicsFrameRate);
        for(int i = 0; i < numberOfIterations; i++)
        {
            PhysicsUpdate(Time.deltaTime / numberOfIterations);
        }
    }

    public void Jump()
    {
        if (m_OnGround)
        {
            m_Velocity.y = m_JumpForce;
            m_JumpingTime = m_JumpUnstickTime;
        }
    }

    /// <summary>
    /// Physics loop
    /// </summary>
    /// <param name="deltaTime"></param>
    private void PhysicsUpdate( float deltaTime)
    {
        // Gravity
        if (!m_OnGround && m_Velocity.y > -m_TerminalVelocity)
        {
            m_Velocity.y += m_Gravity * deltaTime;
        }

        // Movement
        m_Transform.transform.position += m_Velocity * deltaTime;

        // Resolve Collisions
        float pointOffset = m_Collider.height * 0.5f - m_Collider.radius;
        for (int i = 0; i < Physics.defaultSolverIterations; i++)
        {
            int numCol = Physics.OverlapCapsuleNonAlloc(
                m_Transform.TransformPoint(m_Collider.center + (Vector3.up * pointOffset)),
                m_Transform.TransformPoint(m_Collider.center + (Vector3.down * pointOffset)),
                m_Collider.radius,
                m_Collisions,
                m_CollisionMask,
                QueryTriggerInteraction.Ignore);

            if(numCol == 0)
            {
                break;
            }

            Vector3 bestDir = Vector3.zero;
            float bestDist = 0;
            Vector3 colDir;
            float colDist;
            for (int j = 0; j < numCol; j++)
            {
                if(!Physics.ComputePenetration(
                    m_Collider,
                    m_Transform.position,
                    m_Transform.rotation,
                    m_Collisions[j],
                    m_Collisions[j].transform.position,
                    m_Collisions[j].transform.rotation,
                    out colDir,
                    out colDist))
                {
                    continue;
                }

                if(colDist > bestDist)
                {
                    bestDist = colDist;
                    bestDir = colDir;
                }
            }
            m_Transform.position += bestDir * ( bestDist +Physics.defaultContactOffset);
        }


        // Grounding
        RaycastHit hit;
        if (m_JumpingTime <= 0 &&
            Physics.SphereCast(
            m_Transform.TransformPoint(m_Collider.center),
            m_Collider.radius,
            Vector3.down,
            out hit,
            (m_Collider.height * 0.5f) - m_Collider.radius + m_GroundSnapDistance,
            m_CollisionMask))
        {
            m_OnGround = true;
            m_Velocity.y = 0;
            float distanceFromGround = hit.distance + m_Collider.radius - (m_Collider.height * 0.5f);
            m_Transform.transform.position += Vector3.down * distanceFromGround;
        }
        else
        {
            m_OnGround = false;
        }

        m_JumpingTime = Mathf.Max(0, m_JumpingTime - Time.deltaTime);
    }
}
