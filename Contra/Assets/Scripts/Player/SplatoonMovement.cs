﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplatoonMovement : MonoBehaviour
{
    [SerializeField]
    private PlayerPhysics m_Physics;

    [SerializeField]
    private Transform m_Root;

    [SerializeField]
    private float m_Speed = 10;

    [SerializeField]
    private float m_TurnSpeed = 180;

    /// <summary>
    /// TODO To Component
    /// </summary>
    public void DoMovement(Vector3 moveDirection)
    {
        // Movement
        Vector3 velocity = m_Physics.Velocity;
        velocity.x = moveDirection.x * m_Speed;
        velocity.z = moveDirection.z * m_Speed;
        m_Physics.Velocity = velocity;

        // Rotation
        m_Root.Rotate(Vector3.up, Input.GetAxis("CameraX") * m_TurnSpeed * Time.deltaTime, Space.World);
    }
}
