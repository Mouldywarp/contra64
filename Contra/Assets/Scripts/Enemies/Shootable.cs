﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootable : MonoBehaviour
{
    [SerializeField]
    private int m_MaxHealth;

    [SerializeField]
    private ReceiverList m_Receivers;

    public interface IReceiver
    {
        void OnHit();
        void OnDie();
    }

    [System.Serializable]
    private class ReceiverList : ReceiverList<IReceiver>
    {
    }


    private int m_Health;

    private void Awake()
    {
        m_Receivers.Initialize();
    }

    private void OnEnable()
    {
        m_Health = m_MaxHealth;
    }

    public void Shoot(int damage)
    {
        if (m_Health <= 0) return;
        m_Health -= damage;
        for (int i = 0; i < m_Receivers.Count; i++)
        {
            m_Receivers[i].OnHit();
        }
        if (m_Health <= 0)
        {
            m_Health = 0;
            for (int i = 0; i < m_Receivers.Count; i++)
            {
                m_Receivers[i].OnDie();
            }
        }
    }
}





[System.Serializable]
public class ReceiverList<T>
{
    [SerializeField]
    private GameObject[] m_ReceiverObjects;

    private List<T> m_Receivers;

    public T this[int i]
    {
        get { return m_Receivers[i]; }
    }

    public int Count { get { return m_Receivers.Count; } }

    public void Initialize()
    {
        m_Receivers = new List<T>();
        for (int i = 0; i < m_ReceiverObjects.Length; i++)
        {
            T component = m_ReceiverObjects[i].GetComponent<T>();
            if (component != null)
            {
                m_Receivers.Add(component);
            }
        }
    }
}