﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MobController : MonoBehaviour
{
    [SerializeField]
    private NavMeshAgent[] m_Mob;

    [SerializeField]
    private Transform m_Target;

    [SerializeField]
    private float m_MaxDistanceToPlayer = 8.0f;

    [SerializeField]
    private float m_MinDistanceToPlayer = 2.0f;

    private Vector3[] m_OriginalPositions;

    private bool m_PlayerIsInRange;

    private void Start()
    {
        m_OriginalPositions = new Vector3[m_Mob.Length];
        for ( int i = 0; i < m_Mob.Length; i++ )
        {
            m_OriginalPositions[i] = m_Mob[i].transform.position;
        }
    }

    private void Update()
    {
        if ( m_PlayerIsInRange )
        {
            for ( int i = 0; i < m_Mob.Length; i++ )
            {
                Vector3 toPosition = ( m_Mob[i].transform.position - m_Target.position );
                float sqrDist = toPosition.sqrMagnitude;

                // If not between min and max, needs to move
                if ( sqrDist > m_MaxDistanceToPlayer * m_MaxDistanceToPlayer ||
                   sqrDist < m_MinDistanceToPlayer * m_MinDistanceToPlayer )
                {
                    // Destination is halfway bewteen min and max
                    m_Mob[i].destination = m_Target.position +
                        ( toPosition.normalized * ( m_MinDistanceToPlayer + m_MaxDistanceToPlayer ) * 0.5f );
                }
            }
        }
        else
        {
            int numHome = 0;
            for ( int i = 0; i < m_Mob.Length; i++ )
            {
                if((m_Mob[i].transform.position - m_OriginalPositions[i]).sqrMagnitude < 0.3f)
                {
                    numHome++;
                }
                else
                {
                    m_Mob[i].destination = m_OriginalPositions[i];
                }
            }
        }
    }

    private void OnTriggerEnter( Collider other )
    {
        m_PlayerIsInRange = true;
    }

    private void OnTriggerExit( Collider other )
    {
        m_PlayerIsInRange = false;
    }





}
