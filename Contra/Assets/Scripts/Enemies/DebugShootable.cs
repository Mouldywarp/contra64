﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugShootable : MonoBehaviour, Shootable.IReceiver
{
    List<Material> m_Materials;

    private IEnumerator m_HitRoutine;

    void OnEnable()
    {
        m_HitRoutine = null;
    }

    private void Awake()
    {
        m_Materials = new List<Material>();
        foreach (var renderer in GetComponentsInChildren<Renderer>())
        {
            m_Materials.Add(renderer.material);
        }

    }

    public void OnHit()
    {
        if (gameObject.activeInHierarchy && m_HitRoutine == null)
        {
            m_HitRoutine = HitRoutine();
            StartCoroutine(m_HitRoutine);
        }


    }

    public void OnDie()
    {

    }

    private IEnumerator HitRoutine()
    {
        for (int i = 0; i < m_Materials.Count; i++)
        {
            m_Materials[i].color = Color.red;
        }
        yield return new WaitForSeconds(0.05f);
        for (int i = 0; i < m_Materials.Count; i++)
        {
            m_Materials[i].color = Color.white;
        }
        m_HitRoutine = null;
    }
}
